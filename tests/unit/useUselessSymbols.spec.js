import {ref} from "vue";
import useUnitRules from "../../src/composables/useUnitRules";
import useUselessSymbols from "../../src/composables/useUselessSymbols";

describe('useUselessSymbols.spec.js', () => {
    it('eliminates rules that contain useless symbols from example 8.1', () => {

        const nonterminals = ref(["S", "A", "B", "C"])
        const terminals = ref(["1", "2"])
        const startVariable = ref("S")
        const rules = ref([
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "BC"
            },
            {
                lhs: "S",
                rhs: "B2"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "A",
                rhs: "BC"
            },
            {
                lhs: "A",
                rhs: "B2"
            },
            {
                lhs: "A",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: "2"
            }
        ])

        const { eliminatedUselessRules, expectedUselessVars, getEliminatedUselessRules } = useUselessSymbols(rules, nonterminals, terminals, startVariable);

        getEliminatedUselessRules()
        expect(eliminatedUselessRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "B2"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: "2"
            }
        ])
    })
})

describe('useUselessSymbols.spec.js', () => {
    it('eliminates useless rules from exercise 8.4', () => {

        const nonterminals = ref(["S"])
        const terminals = ref(["(", ")"])
        const startVariable = ref("S")

        const rules = ref([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            },
            {
                lhs: "S",
                rhs: "()"
            }
        ])

        const { eliminatedUselessRules, expectedUselessVars, getEliminatedUselessRules } = useUselessSymbols(rules, nonterminals, terminals, startVariable);

        getEliminatedUselessRules()
        expect(eliminatedUselessRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            },
            {
                lhs: "S",
                rhs: "()"
            }
        ])
    })
})

describe('useUselessSymbols.spec.js', () => {
    it('eliminates useless rules from example 8.2', () => {

        const nonterminals = ref(["S", "A","B", "C"])
        const terminals = ref(["0", "1", "2"])
        const startVariable = ref("S")

        const rules = ref([
            {
                lhs: "S",
                rhs: "0B"
            },
            {
                lhs: "S",
                rhs: "C1"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "A",
                rhs: "C1"
            },
            {
                lhs: "A",
                rhs: "1"
            },
            {
                lhs: "A",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "C1"
            },
            {
                lhs: "B",
                rhs: "1"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "C",
                rhs: "1"
            },
            {
                lhs: "C",
                rhs: "2"
            }
        ])

        const { eliminatedUselessRules, expectedUselessVars, getEliminatedUselessRules } = useUselessSymbols(rules, nonterminals, terminals, startVariable);

        getEliminatedUselessRules()
        expect(eliminatedUselessRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "0B"
            },
            {
                lhs: "S",
                rhs: "C1"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "C1"
            },
            {
                lhs: "B",
                rhs: "1"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "C",
                rhs: "1"
            },
            {
                lhs: "C",
                rhs: "2"
            }
        ])
    })
})