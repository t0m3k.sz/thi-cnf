import {mount, shallowMount} from "@vue/test-utils";
import {ref} from "vue";
import useEpsilonRules from "../../src/composables/useEpsilonRules";

describe('useEpsilonRules.spec.js', () => {
    it('eliminates empty rules from example 8.1', () => {

        const rules = ref([
            {
                lhs: "S",
                rhs: "A"
            },
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "A",
                rhs: "BC"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: ""
            },
            {
                lhs: "C",
                rhs: ""
            }
        ])

        const { eliminatedEpsilonRules, getEliminatedEpsilonRules } = useEpsilonRules(rules);
        getEliminatedEpsilonRules()

        expect(eliminatedEpsilonRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "A"
            },
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "A",
                rhs: "BC"
            },
            {
                lhs: "A",
                rhs: "C"
            },
            {
                lhs: "A",
                rhs: "B"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: "2"
            }
        ])
    })
})

describe('useEpsilonRules.spec.js', () => {
    it('eliminates empty rules from exercise 8.4', () => {

        const rules = ref([
            {
                lhs: "S",
                rhs: ""
            },
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            }
        ])

        const { eliminatedEpsilonRules, getEliminatedEpsilonRules } = useEpsilonRules(rules);
        getEliminatedEpsilonRules()

        expect(eliminatedEpsilonRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            },
            {
                lhs: "S",
                rhs: "S"
            },
            {
                lhs: "S",
                rhs: "()"
            }
        ])

    })
})

describe('useEpsilonRules.spec.js', () => {
    it('eliminates empty rules from example 8.4', () => {

        const rules = ref([
            {
                lhs: "S",
                rhs: "aBbC"
            },
            {
                lhs: "S",
                rhs: "a"
            },
            {
                lhs: "B",
                rhs: "bC"
            },
            {
                lhs: "C",
                rhs: "c"
            }
        ])

        const { eliminatedEpsilonRules, getEliminatedEpsilonRules } = useEpsilonRules(rules);
        getEliminatedEpsilonRules()

        expect(eliminatedEpsilonRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "aBbC"
            },
            {
                lhs: "S",
                rhs: "a"
            },
            {
                lhs: "B",
                rhs: "bC"
            },
            {
                lhs: "C",
                rhs: "c"
            }
        ])

    })
})