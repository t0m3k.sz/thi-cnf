import {ref} from "vue";
import useUnitRules from "../../src/composables/useUnitRules";
import useUselessSymbols from "../../src/composables/useUselessSymbols";
import useNonsolitaryTerminals from "../../src/composables/useNonsolitaryTerminals";
import useMoreThanTwo from "../../src/composables/useMoreThanTwo";

describe('useNonsolitaryTerminals.spec.js', () => {
    it('eliminates rules that contain more than two nonterminals from example 8.1', () => {

        const rules = ref([
            {
                lhs: "S",
                rhs: "XB"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "BY"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "BY"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "X",
                rhs: "1"
            },
            {
                lhs: "Y",
                rhs: "2"
            }
        ])

        const { eliminatedMoreThanTwoNonterminals, getEliminatedMoreThanTwoTerminals } = useMoreThanTwo(rules);

        getEliminatedMoreThanTwoTerminals()

        expect(eliminatedMoreThanTwoNonterminals.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "XB"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "BY"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "BY"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "X",
                rhs: "1"
            },
            {
                lhs: "Y",
                rhs: "2"
            }
        ])
    })
})

describe('useUselessSymbols.spec.js', () => {
    it('eliminates rules that contain more than two nonterminals from exercise 8.4', () => {

        const rules = ref([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "XSY"
            },
            {
                lhs: "S",
                rhs: "XY"
            },
            {
                lhs: "X",
                rhs: "("
            },
            {
                lhs: "Y",
                rhs: ")"
            }
        ])

        const { eliminatedMoreThanTwoNonterminals, getEliminatedMoreThanTwoTerminals } = useMoreThanTwo(rules);

        getEliminatedMoreThanTwoTerminals()

        expect(eliminatedMoreThanTwoNonterminals.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "XZ"
            },
            {
                lhs: "S",
                rhs: "XY"
            },
            {
                lhs: "X",
                rhs: "("
            },
            {
                lhs: "Y",
                rhs: ")"
            },
            {
                lhs: "Z",
                rhs: "SY"
            }
        ])
    })
})

describe('useMoreThanTwo.spec.js', () => {
    it('eliminates rules that contain more than two nonterminalsfrom example 8.2', () => {

        const nonterminals = ref(["S", "A","B", "C"])
        const terminals = ref(["0", "1", "2"])

        const rules = ref([
            {
                lhs: "S",
                rhs: "XB"
            },
            {
                lhs: "S",
                rhs: "CY"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "CY"
            },
            {
                lhs: "B",
                rhs: "1"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "C",
                rhs: "1"
            },
            {
                lhs: "C",
                rhs: "2"
            },
            {
                lhs: "X",
                rhs: "0"
            },
            {
                lhs: "Y",
                rhs: "1"
            }
        ])

        const { eliminatedMoreThanTwoNonterminals, getEliminatedMoreThanTwoTerminals } = useMoreThanTwo(rules);

        getEliminatedMoreThanTwoTerminals()

        expect(eliminatedMoreThanTwoNonterminals.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "XB"
            },
            {
                lhs: "S",
                rhs: "CY"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "CY"
            },
            {
                lhs: "B",
                rhs: "1"
            },
            {
                lhs: "B",
                rhs: "2"
            },
            {
                lhs: "C",
                rhs: "1"
            },
            {
                lhs: "C",
                rhs: "2"
            },
            {
                lhs: "X",
                rhs: "0"
            },
            {
                lhs: "Y",
                rhs: "1"
            }
        ])
    })
})