import {ref} from "vue";
import useUnitRules from "../../src/composables/useUnitRules";

describe('useUnitRules.spec.js', () => {
    it('eliminates unit rules from example 8.1', () => {

        const nonterminals = ref(["S", "A", "B", "C"])

        const rules = ref([
            {
                lhs: "S",
                rhs: "A"
            },
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "A",
                rhs: "BC"
            },
            {
                lhs: "A",
                rhs: "C"
            },
            {
                lhs: "A",
                rhs: "B"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: "2"
            }
        ])

        const { eliminatedUnitRules, unitRulesWithSets, getEliminatedUnitRules } = useUnitRules(rules, nonterminals);

        getEliminatedUnitRules()
        expect(eliminatedUnitRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "1B"
            },
            {
                lhs: "S",
                rhs: "1"
            },
            {
                lhs: "S",
                rhs: "BC"
            },
            {
                lhs: "S",
                rhs: "B2"
            },
            {
                lhs: "S",
                rhs: "2"
            },
            {
                lhs: "A",
                rhs: "BC"
            },
            {
                lhs: "A",
                rhs: "B2"
            },
            {
                lhs: "A",
                rhs: "2"
            },
            {
                lhs: "B",
                rhs: "B2"
            },
            {
                lhs: "B",
                rhs: "2"
            }
        ])
    })
})

describe('useUnitRules.spec.js', () => {
    it('eliminates unit rules from exercise 8.4', () => {

        const nonterminals = ref(["S"])

        const rules = ref([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            },
            {
                lhs: "S",
                rhs: "S"
            },
            {
                lhs: "S",
                rhs: "()"
            }
        ])

        const { eliminatedUnitRules, unitRulesWithSets, getEliminatedUnitRules } = useUnitRules(rules, nonterminals);

        getEliminatedUnitRules()
        expect(eliminatedUnitRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "SS"
            },
            {
                lhs: "S",
                rhs: "(S)"
            },
            {
                lhs: "S",
                rhs: "()"
            }
        ])
    })
})

describe('useUnitRules.spec.js', () => {
    it('eliminates unit rules from example 8.4', () => {

        const nonterminals = ref(["S", "B", "C"])

        const rules = ref([
            {
                lhs: "S",
                rhs: "aBbC"
            },
            {
                lhs: "S",
                rhs: "a"
            },
            {
                lhs: "B",
                rhs: "bC"
            },
            {
                lhs: "C",
                rhs: "c"
            }
        ])

        const { eliminatedUnitRules, unitRulesWithSets, getEliminatedUnitRules } = useUnitRules(rules, nonterminals);

        getEliminatedUnitRules()
        expect(eliminatedUnitRules.value).toStrictEqual([
            {
                lhs: "S",
                rhs: "aBbC"
            },
            {
                lhs: "S",
                rhs: "a"
            },
            {
                lhs: "B",
                rhs: "bC"
            },
            {
                lhs: "C",
                rhs: "c"
            }
        ])
    })
})