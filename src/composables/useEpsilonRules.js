import {onMounted, ref, watch} from "vue";
import useRulesMap from "./services/useRulesMap";
import subsets from "./services/useSubsets";
import useArrayFromMap from "./services/useArrayFromMap";

/**
 * Contains all functions to eliminate empty rules
 * @param rules
 * @returns {{eliminatedEpsilonRules: Ref<UnwrapRef<*[]>>, getEliminatedEpsilonRules: getEliminatedEpsilonRules}}
 */
const useEpsilonRules = (rules) => {

    const eliminatedEpsilonRules = ref([]);

    const getEliminatedEpsilonRules = () => {
        let rulesMap = useRulesMap(rules.value)
        let emptyLHS = []

        /**
         * Determines empty LHSs and adds them to the emptyLHS array
         */
        const getEpsilonRules = () => {
            for (const [key, value] of rulesMap.entries()) {
                if(value.has("")) {
                    emptyLHS.push(key)
                }
            }}

        /**
         * Determines all indexes of a certain value within an array
         * @param arr
         * @param val
         * @returns {*[]}
         */
        const getAllIndexes = (arr, val) => {
            let indexes = [], i;
            for(i = 0; i < arr.length; i++)
                if (arr[i] === val)
                    indexes.push(i);
            return indexes;
        }

        /**
         * Generates new production rules
         * @param possibleCombinations
         * @param key
         * @param rhs
         */
        const addNewRules = (possibleCombinations, key, rhs) => {
            for (let possibility of possibleCombinations) {
                let rhsArr = rhs.split("")

                possibility.forEach(i => {
                    rhsArr.splice(i, 1);
                })
                let newRule = rhsArr.join("");
                rulesMap.get(key).add(newRule)
            }
        }

        /**
         * Compute possible combinations for rules which contain an empty variable
         * @param key
         * @param lhs
         * @param rhs
         */
        const computePossibilities = (key, lhs, rhs) => {
            let possibleCombinations = [];

            const indexes = getAllIndexes(rhs.split(""), lhs);

            // generate subsets of the found indexes and push them to the possibleCombinations array
            for (let subset of subsets(indexes)) {
                possibleCombinations.push(subset);
            }

            // add new rules
            addNewRules(possibleCombinations, key, rhs);
        }

        // determine each rhs that contains an empty lhs
        const determineEmptyRhs = () => {
            emptyLHS.forEach(lhs => {
                for (const [key, value] of rulesMap.entries()) {
                    value.forEach(rhs => {
                        if(rhs.indexOf(lhs) > -1) {
                            computePossibilities(key, lhs, rhs)
                        }
                    })
                }})
        }

        getEpsilonRules()
        determineEmptyRhs()

        const result = useArrayFromMap(rulesMap)
        // get rid off empty rules
        eliminatedEpsilonRules.value = result.filter(rule => !rule.rhs == '')
    }

    return {
        eliminatedEpsilonRules,
        getEliminatedEpsilonRules
    }

}

export default useEpsilonRules