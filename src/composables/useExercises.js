import {computed, onMounted, ref, watch} from "vue";

const useExercises = () => {
    const exercises = ref(null)
    const error = ref(null);

    const getExercises = () => {
        fetch("/data/cnf.json")
            .then(res => res.text())
            .then(text => JSON.parse(text))
            .then(data => data.exercises)
            .then(exerciseData => exercises.value = exerciseData)
            .catch(err => error.value = err)
    }

    const classExercisesList = computed(() => {
        if(exercises.value) {
            return exercises.value
                .filter(exercise => exercise.category == "Vorlesung")
                .map(exercise => {
                    return {
                        id: exercise.id,
                        shortId: getShortId(exercise.id)
                    }
                })
        }
    })

    const getShortId = (id) => {
        let index = id.indexOf('-')
        return id.substr(0, index)
    }

    const exerciseExercisesList = computed(() => {
        if(exercises.value) {
            return exercises.value
                .filter(exercise => exercise.category == "Übung")
                .map(exercise => {
                    return {
                        id: exercise.id,
                        shortId: getShortId(exercise.id)
                    }
                })
        }
    })

    onMounted(getExercises)

    return {
        exercises,
        error,
        getExercises,
        classExercisesList,
        exerciseExercisesList
    }

}

export default useExercises