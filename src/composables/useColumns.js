import {onMounted, onUpdated, ref, watch} from "vue";
import useEpsilonRules from "./useEpsilonRules";
import useUnitRules from "./useUnitRules";
import useUselessSymbols from "./useUselessSymbols";
import useNonsolitaryTerminals from "./useNonsolitaryTerminals";
import useMoreThanTwo from "./useMoreThanTwo";

const useColumns = (id, rules, nonterminals, terminals, startVariable) => {

    const columns = ref([])

    const getColumns = () => {

        const { eliminatedEpsilonRules, getEliminatedEpsilonRules } = useEpsilonRules(rules)
        getEliminatedEpsilonRules()

        const { eliminatedUnitRules, unitRulesWithSets, getEliminatedUnitRules } = useUnitRules(eliminatedEpsilonRules, nonterminals)
        getEliminatedUnitRules()

        const { eliminatedUselessRules, expectedUselessVars, getEliminatedUselessRules } = useUselessSymbols(eliminatedUnitRules, nonterminals, terminals, startVariable)
        getEliminatedUselessRules()

        const { eliminatedNonsolitaryTerminals, getEliminatedNonsolitaryTerminals } = useNonsolitaryTerminals(eliminatedUselessRules, terminals, nonterminals)
        getEliminatedNonsolitaryTerminals()

        const { eliminatedMoreThanTwoNonterminals, getEliminatedMoreThanTwoTerminals } = useMoreThanTwo(eliminatedNonsolitaryTerminals)
        getEliminatedMoreThanTwoTerminals()

        columns.value = [
            {
                title: "Produktionen",
                step: 0,
                rules: rules.value,
                expected: [],
                isEditable: false
            },
            {
                title: "ε-Regeln",
                step: 1,
                rules: [],
                expected: eliminatedEpsilonRules.value,
                isEditable: true,
                state: 'editing'
            },
            {
                title: "Kettenregeln",
                step: 2,
                rules: [],
                expected: eliminatedUnitRules.value,
                expectedUnitRulesWithSets: unitRulesWithSets,
                unitRulesWithSets: [],
                isEditable: true,
                state: 'locked'
            },
            {
                title: "Nutzlose Variablen",
                step: 3,
                rules: [],
                uselessVars: [],
                expected: eliminatedUselessRules.value,
                expectedUselessVars: expectedUselessVars,
                isEditable: true,
                state: 'locked'
            },
            {
                title: "Separation der Terminalzeichen",
                step: 4,
                rules: [],
                expected: eliminatedNonsolitaryTerminals.value,
                previousRules: eliminatedUselessRules.value,
                isEditable: true,
                state: 'locked'
            },
            {
                title: "Elimination von Variablenfolgen",
                step: 5,
                rules: [],
                expected: eliminatedMoreThanTwoNonterminals.value,
                isEditable: true,
                state: 'locked'
            }
        ]
    }

    const unlockNextStep = (step) => {
        const columnIndex = columns.value.findIndex(col => col.step === step)
        columns.value[columnIndex].state = 'editing'
    }

    onMounted(getColumns)
    watch(id, getColumns)

    return {
        columns,
        getColumns,
        unlockNextStep
    }
}

export default useColumns