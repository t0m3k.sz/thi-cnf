import {onMounted, ref, watch} from "vue";
import useRulesMap from "./services/useRulesMap";
import useArrayFromMap from "./services/useArrayFromMap";

/**
 * Contains functions to eliminate rules which contain nonsolitary terminals
 * @param rules
 * @param terminals
 * @param nonterminals
 * @returns {{getEliminatedNonsolitaryTerminals: getEliminatedNonsolitaryTerminals, eliminatedNonsolitaryTerminals: Ref<UnwrapRef<*[]>>}}
 */
const useNonsolitaryTerminals = (rules, terminals, nonterminals) => {

    const eliminatedNonsolitaryTerminals = ref([])
    const varPool = "XYZUVWPQRSTABCDEFGHIJKLMNO".split("").filter(char => !nonterminals.value.includes(char))

    /**
     * Checks if the right-hand side contains nonterminals
     * @param rhs
     * @returns {boolean}
     */
    const containsNonterminal = (rhs) => {
        return rhs.split("").some(sym => nonterminals.value.includes(sym))
    }
    /**
     * Checks if the right-hand side contains terminals
     * @param rhs
     * @returns {boolean}
     */
    const containsTerminal = (rhs) => {
        return rhs.split("").some(sym => terminals.value.includes(sym))
    }

    /**
     * Checks if a rule is in Chomsky normal form
     * @param rhs
     * @returns {boolean}
     */
    const isCNF = (rhs) => {
        const isSingleTerminal = rhs.length == 1 && containsTerminal(rhs)
        const isTwoNonterminals = rhs.length == 2 && rhs.split("").every(sym => containsNonterminal(rhs))

        return isSingleTerminal && isTwoNonterminals
    }

    /**
     * Check if the passed righ-hand side contains nonsolitary terminals
     * @param rhs
     * @returns {boolean}
     */
    const containsNonsolitaryTerminals = (rhs) => {
        return !isCNF(rhs) && containsTerminal(rhs) && rhs.length > 1
    }

    /**
     * Get the set of nonsolitary terminals
     * @param rules
     * @returns {Set<any>}
     */
    const getNonsolitaryTerminalsSet = (rules) => {
        const nonsolitaryTerminalsSet = new Set()

        rules.forEach(rule => {
            // check if rule contains nonsolitary terminals
            if(containsNonsolitaryTerminals(rule.rhs)) {
                rule.rhs.split("").forEach(sym => {
                    if(terminals.value.includes(sym)) {
                        nonsolitaryTerminalsSet.add(sym)
                    }
                })
            }
        })

        return nonsolitaryTerminalsSet
    }

    /**
     * The main function of the composable.
     * Determine the result and set it to the eliminatedNonsolitaryTerminals ref
     */
    const getEliminatedNonsolitaryTerminals = () => {

        const rulesMap = useRulesMap(rules.value)
        const nonsolitaryTerminals = getNonsolitaryTerminalsSet(rules.value)
        const terminalVarMap = new Map()

        /**
         * Add a new rule for each nonsolitary terminal
         */
        const addNewRules = () => {
            nonsolitaryTerminals.forEach(terminal => {
                let newVar = varPool.shift()
                terminalVarMap.set(terminal, newVar)
                rulesMap.set(newVar, new Set().add(terminal))
            })
        }

        /**
         * Replace nonsolitary terminals by the new variable within a right-hand side
         * @param rhs
         * @returns {*}
         */
        const replaceNonsolitaryTerminals = (rhs) => {
            let newRhs = rhs
            Array.from(terminalVarMap.keys()).forEach(terminal => {
                if(newRhs.includes(terminal)) {
                    newRhs = newRhs.replaceAll(terminal, terminalVarMap.get(terminal))
                }
            })
            return newRhs
        };

        /**
         * Replace nonsolitary terminals by the new variables
         * @param rules
         * @returns {*}
         */
        const replaceAllNonsolitaryTerminals = (rules) => {
            return rules.map(rule => {
                if(containsNonsolitaryTerminals(rule.rhs)) {
                    return ({
                        lhs: rule.lhs,
                        rhs: replaceNonsolitaryTerminals(rule.rhs)
                    })
                } else {
                    return ({
                        lhs: rule.lhs,
                        rhs: rule.rhs
                    })
                }
            })
        }

        addNewRules()
        const result = replaceAllNonsolitaryTerminals(useArrayFromMap(rulesMap))
        eliminatedNonsolitaryTerminals.value = result
    }

    return {
        eliminatedNonsolitaryTerminals,
        getEliminatedNonsolitaryTerminals
    }
}

export default useNonsolitaryTerminals