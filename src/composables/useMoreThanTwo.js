import {ref} from "vue";
import useRulesMap from "./services/useRulesMap";
import useArrayFromMap from "./services/useArrayFromMap";

/**
 * Contains functions to eliminate rules which contain more than two nonterminals
 * @param rules
 * @returns {{eliminatedMoreThanTwoNonterminals: Ref<UnwrapRef<*[]>>, getEliminatedMoreThanTwoTerminals: getEliminatedMoreThanTwoTerminals}}
 */
const useMoreThanTwo = (rules) => {
    const eliminatedMoreThanTwoNonterminals = ref ([])
    const rulesMap = useRulesMap(rules.value)
    const newNonterminals = Array.from(rulesMap.keys())
    const varPool = "XYZUVWPQRSTABCDEFGHIJKLMNO".split("").filter(char => !newNonterminals.includes(char))

    /**
     * Determines if a right-hand side contains more than two nonterminals
     * @param rhs
     * @returns {boolean}
     */
    const containsMoreThanTwoNonterminals = (rhs) => {
        return rhs.split("").filter(sym => sym === sym.toUpperCase()).length > 2
    }

    /**
     * Replaces a rule by a new rule without more than two nonterminals
     * @param rhs
     * @param newRulesMapReversed
     * @returns {*}
     */
    const replaceByNewRule = (rhs, newRulesMapReversed) => {
        let newRhs = rhs.replace(rhs.substring(1), newRulesMapReversed.get(rhs.substring(1)))
        return newRhs
    }

    /**
     * Main function of the composable.
     * The Result is set to the eliminatdMoreThanTwoNonterminals ref
     */
    const getEliminatedMoreThanTwoTerminals = () => {

        const newRulesMapReversed = new Map()

        /**
         * Add a new rule to the reversed map of rules
         * @param rhs
         */
        const addNewRule = (rhs) => {
            let newRule = rhs.substring(1)
            while (newRule.length >= 2) {
                let newVar = varPool.shift()
                rulesMap.set(newVar, new Set().add(newRule))
                newRulesMapReversed.set(newRule, newVar)
                newRule = newRule.substring(1)
            }
        }

        /**
         * Add a new rule for each rule that contains more than two nonterminals
         */
        rules.value.forEach(rule =>  {
            if(containsMoreThanTwoNonterminals(rule.rhs)) {
                addNewRule(rule.rhs)
            }
        })

        const newRules = useArrayFromMap(rulesMap)


        const result = newRules.map(rule => {
            if(containsMoreThanTwoNonterminals(rule.rhs)) {
                return {
                    lhs: rule.lhs,
                    rhs: replaceByNewRule(rule.rhs, newRulesMapReversed)
                }
            } else {
                return {
                    lhs: rule.lhs,
                    rhs: rule.rhs
                }
            }
        })

        eliminatedMoreThanTwoNonterminals.value = result
    }

    return {
        eliminatedMoreThanTwoNonterminals,
        getEliminatedMoreThanTwoTerminals
    }

}

export default useMoreThanTwo