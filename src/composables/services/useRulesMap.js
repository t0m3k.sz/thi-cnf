const useRulesMap = (rules) => {

    let resultMap = new Map();

    rules.forEach(rule => {
        if(resultMap.has(rule.lhs)) {
            resultMap.get(rule.lhs).add(rule.rhs)
        } else {
            const newSet = new Set()
            resultMap.set(rule.lhs, newSet.add(rule.rhs))
        }
    })

    return resultMap;
}

export default useRulesMap