const useTransitionsMap = (rulesMap) => {

    let transitionsMap = rulesMap;

    for (const [lhs, rhs] of rulesMap.entries()) {
        rulesMap.get(lhs).forEach(rhs => {
            if(transitionsMap.get(rhs)) {
                transitionsMap.get(rhs).forEach(elem => {
                    transitionsMap.get(lhs).add(elem)
                })
            }
        })
    }
    return transitionsMap;
}

export default useTransitionsMap