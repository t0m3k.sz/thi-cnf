/**
 * Returns a array for the passed map.
 * All keys are being set as LHS properties.
 * All members of the sets are set as RHS properties of the production rules
 * @param rulesMap
 * @returns {*[]}
 */
const useArrayFromMap = (rulesMap) => {
    let resultArr = []
    for (const [key, set] of rulesMap.entries()) {
        set.forEach(rhs => {
            resultArr.push({ lhs: key, rhs })
        })

    }
    return resultArr
}

export default useArrayFromMap