import {onMounted, ref, watch} from "vue";

const useExercise = () => {
    const chosenExercise = ref("8.1-V")
    const exercise = ref(null)
    const error = ref(null);

    const getExercise = () => {
        fetch("/data/cnf.json")
            .then(res => res.text())
            .then(text => JSON.parse(text))
            .then(data => data.exercises.find(exercise => exercise.id == chosenExercise.value))
            .then(exerciseData => exercise.value = exerciseData)
            .catch(err => error.value = err)

    }

    onMounted(getExercise)

    return {
        exercise,
        chosenExercise,
        error,
        getExercise
    }

}

export default useExercise

