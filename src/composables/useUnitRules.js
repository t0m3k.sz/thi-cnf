import {onMounted, ref, watch} from "vue";
import useTransitionsMap from "./services/useTransitionsMap";
import useRulesMap from "./services/useRulesMap";
import useArrayFromMap from "./services/useArrayFromMap";

/**
 * Contains functions to eliminate unit rules
 * @param rules
 * @param nonterminals
 * @returns {{getEliminatedUnitRules: getEliminatedUnitRules, unitRulesWithSets: Ref<UnwrapRef<*[]>>, eliminatedUnitRules: Ref<UnwrapRef<*[]>>}}
 */
const useUnitRules = (rules, nonterminals) => {
    const eliminatedUnitRules = ref([])
    const unitRulesWithSets = ref([])

    const getUnitRules = () => {

        // determine unit rules
        const unitRules = rules.value
            .filter(rule => {
                return rule.rhs.length === 1
                    && rule.rhs == rule.rhs.toUpperCase()
                    && nonterminals.value.includes(rule.rhs)
            })

        return unitRules
    }

    /**
     * Determine unit rules with their unit sets
     * @param unitRulesTransitionsMap
     * @param rulesMap
     * @returns {{set, sym: *}[]}
     */
    const getUnitRulesWithSets = (unitRulesTransitionsMap, rulesMap) => {
        const nonterminalsInTransitionMap = [...unitRulesTransitionsMap.keys()]
        const nonterminalsInRulesMap = [...rulesMap.keys()]

        const unitRulesWithSets = Array.from(unitRulesTransitionsMap).map(([sym, set]) => ({ sym, set: [...set] }))

        nonterminals.value.forEach(nonterminal => {
            if(!nonterminalsInTransitionMap.includes(nonterminal) && nonterminalsInRulesMap.includes(nonterminal)) {
                unitRulesWithSets.push({
                    sym: nonterminal,
                    set: []
                })
            }
        })

        return unitRulesWithSets;
    }

    /**
     * Main function of the composable.
     * The Result is set to the eliminatdUnitRules ref
     */
    const getEliminatedUnitRules = () => {
        let rulesMap = useRulesMap(rules.value)

        const unitRules = getUnitRules();
        const unitRulesMap = useRulesMap(unitRules);
        const unitRulesTransitionsMap = useTransitionsMap(unitRulesMap);

        // Remove unit rules
        unitRules.forEach(unitRule => {
            rulesMap.get(unitRule.lhs).delete(unitRule.rhs)
        })

        // Replace unit rules by new production rules
        for (const [lhs, rhsSet] of unitRulesTransitionsMap.entries()) {
            rhsSet.forEach(rhs => {
                if(rulesMap.get(rhs)) {
                    rulesMap.get(rhs).forEach(rule => {
                        // check if the rule is a unit rule
                        if(rule.length > 1 || !nonterminals.value.includes(rule)) {
                            // add a new rule for each unit rule
                            rulesMap.get(lhs).add(rule)
                        }
                    })
                }
            })
        }
        eliminatedUnitRules.value = useArrayFromMap(rulesMap)
        unitRulesWithSets.value = getUnitRulesWithSets(unitRulesTransitionsMap, rulesMap)
    }

    return {
        eliminatedUnitRules,
        unitRulesWithSets,
        getEliminatedUnitRules
    }

}

export default useUnitRules