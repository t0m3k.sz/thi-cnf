import {onMounted, ref, watch} from "vue"
import useRulesMap from "./services/useRulesMap";
import useArrayFromMap from "./services/useArrayFromMap";

/**
 * Contains functions to eliminate rules that contain useless symbols
 * @param rules
 * @param nonterminals
 * @param terminals
 * @param startVariable
 * @returns {{expectedUselessVars: Ref<UnwrapRef<*[]>>, getEliminatedUselessRules: getEliminatedUselessRules, eliminatedUselessRules: Ref<UnwrapRef<*[]>>}}
 */
const useUselessSymbols = (rules, nonterminals, terminals, startVariable) => {

    const eliminatedUselessRules = ref([])
    const expectedUselessVars = ref([])

    /**
     * Determine the set of all symbols
     * @param rules
     * @returns {Set<*>}
     */
    const getSymbolsSet = (rules) => {

        const set = new Set([])

        rules.forEach(rule => {
            rule.rhs.split("").forEach(rhsChar => {
                set.add(rhsChar)
                set.add(rule.lhs)
            })
        })

        return set;
    }

    /**
     * Check if right-hand side consists of terminals only
     * @param rhs
     * @returns {*}
     */
    const hasTerminalsOnly = (rhs) => {
        return rhs.split("").every(sym => terminals.value.includes(sym))
    }

    /**
     * Check if right-hand side consists of terminating symbols only
     * @param rhs
     * @param rulesMap
     * @returns {*}
     */
    const hasTerminatingOnly = (rhs, rulesMap) => {
        return rhs.split("").every(sym => {
            if(rulesMap.get(sym)) {
                return Array.from(rulesMap.get(sym)).some(rhs => hasTerminalsOnly(rhs))
            } else {
                return true
            }
        })
    }

    /**
     * Check if symbol is terminating
     * @param symbol
     * @param rulesMap
     * @returns {boolean}
     */
    const isTerminating = (symbol, rulesMap) => {

        let result = false;
        if(rulesMap.get(symbol)) {

        const rhsArr = Array.from(rulesMap.get(symbol))
            result = rhsArr.some(rhs => hasTerminalsOnly(rhs) || hasTerminatingOnly(rhs, rulesMap))
        }
        return result
    }

    /**
     * Determine the set of right-hand sides
     * @param rulesMap
     * @returns {Set<*>}
     */
    const getRhsSet = (rulesMap) => {
        const rhsSet = new Set([])

        for (const [lhs, set] of rulesMap.entries()) {
            set.forEach(rhs => {
                rhsSet.add(rhs);
            })
        }

        return rhsSet;
    }

    /**
     * Check if symbol is reachable
     * @param symbol
     * @param rulesMap
     * @returns {boolean|boolean}
     */
    const isReachable = (symbol, rulesMap) => {

        const rhsSet = getRhsSet(rulesMap)
        let isReachable = Array.from(rhsSet)
            .some(rhs => rhs.includes(symbol))
        return symbol == startVariable.value ? true : isReachable;
    }

    /**
     * Returns a set of all useless symbols
     * @param rules
     * @param rulesMap
     * @returns {Set<*>}
     */
    const getUselessSymbols = (rules, rulesMap) => {
        const uselessSymbols = new Set([])
        const symbols = Array.from(getSymbolsSet(rules)).filter(sym => nonterminals.value.includes(sym))

        // find all symbols that are not generating or not reachable
        symbols.forEach(symbol => {
            if(!isTerminating(symbol, rulesMap) || !isReachable(symbol, rulesMap)) {
                uselessSymbols.add(symbol)
            }
        })

        return uselessSymbols;
    }

    /**
     * Determine all useless variables
     * @param uselessSymbols
     * @param rulesMap
     * @returns {*[]}
     */
    const getExpectedUselessVars = (uselessSymbols, rulesMap) => {
        const varsArr = []

        uselessSymbols.forEach(symbol => {
            varsArr.push({
                symbol: symbol,
                isNotTerminating: !isTerminating(symbol, rulesMap),
                isUnreachable: !isReachable(symbol, rulesMap)
            })
        })

        return varsArr
    }

    /**
     * Main function of the composable.
     * The Result is set to the eliminatedUselessRules ref
     */
    const getEliminatedUselessRules = () => {
        let rulesMap = useRulesMap(rules.value)

        const uselessSymbols = getUselessSymbols(rules.value, rulesMap)

        // filter out all useless rules
        const result = useArrayFromMap(rulesMap)
            .filter(rule => {
                const isWithinRhs = rule.rhs.split("").some(char => uselessSymbols.has(char))
                return !(uselessSymbols.has(rule.lhs) || isWithinRhs)
            })

        eliminatedUselessRules.value = result
        expectedUselessVars.value = getExpectedUselessVars(uselessSymbols, rulesMap)
    }

    return {
        eliminatedUselessRules,
        getEliminatedUselessRules,
        expectedUselessVars
    }
}

export default useUselessSymbols