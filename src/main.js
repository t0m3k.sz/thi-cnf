import { createApp } from 'vue'
import App from './App.vue'

// import styles
import './assets/styles/scss/style.scss'

createApp(App).mount('#app')